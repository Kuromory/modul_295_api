using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class AusgelieheneBuecherController : ControllerBase
{
    private readonly AusgelieheneBuecherService _service;
    private readonly string table = "rented_books";
    public AusgelieheneBuecherController(AusgelieheneBuecherService service)
    {
        _service = service;
    }

    [HttpGet]
    public async Task<IEnumerable<AusgeliehenesBuch>> Get()
    {
        return await _service.FindAll(table);
    }
    [HttpGet("{id}", Name = "FindOneAusgeliehenesBuch")]
    public async Task<ActionResult<AusgeliehenesBuch>> Get(int id)
    {
        var result = await _service.FindOne(table, id);
        if (result != default)
        {
            return Ok(result);
        }
        return NotFound();
    }
    [HttpPost]
    public async Task<ActionResult<AusgeliehenesBuch>> Insert(AusgeliehenesBuchDto dto)
    {
        if (dto.Id != null)
        {
            return BadRequest("Id cannot be set for insert action.");
        }
        var rentedBook = new AusgeliehenesBuch();
        rentedBook.BuchId = dto.BuchId;
        rentedBook.BenutzerId = dto.BenutzerId;
        rentedBook.Ausleihdatum = dto.Ausleihdatum;
        rentedBook.Rueckgabedatum = dto.Rueckgabedatum;
        var id = await _service.Insert(table, rentedBook);
        if (id != default)
        {

            return CreatedAtRoute("FindOneAusgeliehenesBuch", new { id = id }, dto);
        }

        return BadRequest();
    }
    [HttpPut]
    public async Task<ActionResult<AusgeliehenesBuch>> Update(AusgeliehenesBuchDto dto)
    {
        if (dto.Id == null)
        {
            return BadRequest("Id should be set for insert action.");
        }
        var rentedBook = new AusgeliehenesBuch();
        rentedBook.Id = dto.Id;
        rentedBook.BuchId = dto.BuchId;
        rentedBook.BenutzerId = dto.BenutzerId;
        rentedBook.Ausleihdatum = dto.Ausleihdatum;
        rentedBook.Rueckgabedatum = dto.Rueckgabedatum;
        var id = await _service.Update(table, rentedBook);
        if (id > 0)
        {

            return CreatedAtRoute("FindOneAusgeliehenesBuch", new { id = id }, dto);
        }

        return NotFound();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<AusgeliehenesBuch>> Delete(int id)
    {
        var result = await _service.Delete(table, id);
        if (result > 0)
        {

            return Ok(result);
        }
        
        return NotFound();
    }
}