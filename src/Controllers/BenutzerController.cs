﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;


[Route("api/[controller]")]
[ApiController]
public class BenutzerController : ControllerBase
{
    private readonly IConfiguration _configuration;
    private readonly BenutzerService _service;

    private readonly string table = "users";
    private readonly string issuer = "__NAME__";
    private readonly string audience = "__AUDIENCE__";



    public BenutzerController(IConfiguration configuration, BenutzerService service)
    {

        _configuration = configuration;
        _service = service;
    }

    [HttpPost("register")]
    public async Task<ActionResult<Benutzer>> Register(BenutzerDto request)
    {
        string passwordHash = BCrypt.Net.BCrypt.HashPassword(request.Password);
        var result = await _service.FindAll(table, x => x.Benutzername == request.Benutzername);
        if (!result.IsNullOrEmpty()) return BadRequest("Benutzer gibt es schon");
        var user = new Benutzer();
        user.Name = request.Name;

        user.Adresse = request.Adresse;
        user.Kontaktnummer = request.Kontaktnummer;
        user.Benutzername = request.Benutzername;
        user.Password = passwordHash;

        var isSuccess = await _service.Insert(table, user);

        return Ok(user);
    }

    [HttpPost("login")]
    public async Task<ActionResult<Benutzer>> Login(BenutzerDto request)
    {

        var result = await _service.FindAll(table, x => x.Benutzername == request.Benutzername);

        if (result.IsNullOrEmpty()) return BadRequest("Benutzer nicht gefunden");

        Benutzer user = result.First();

        if (!BCrypt.Net.BCrypt.Verify(request.Password, user.Password))
        {
            return BadRequest("Falsches Passwort.");
        }

        string token = CreateToken(user);

        return Ok(token);
    }

    private string CreateToken(Benutzer user)
    {
        List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Benutzername)
            };

        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value!));

        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        var token = new JwtSecurityToken(
                claims: claims,
                signingCredentials: creds,
                audience: audience,
                issuer: issuer,
                expires: DateTime.Now.AddDays(1)
            );

        return new JwtSecurityTokenHandler().WriteToken(token);

    }
    
    protected bool ValidateToken(string authToken)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = GetTokenValidationParameters();

            SecurityToken validatedToken;
            IPrincipal principal = tokenHandler.ValidateToken(authToken, validationParameters, out validatedToken);
            return true;
        }
        catch (System.Exception)
        {
            return false;
        }
    }
    private TokenValidationParameters GetTokenValidationParameters()
    {
        return new TokenValidationParameters()
        {
            ValidateLifetime = false,
            ValidateAudience = false,
            ValidateIssuer = false,
            ValidIssuer = issuer,
            ValidAudience = audience,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value!))
        };
    }
}

