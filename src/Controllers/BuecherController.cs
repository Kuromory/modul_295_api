using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class BuecherController : ControllerBase
{
    private readonly BuecherService _service;
    private readonly string table = "books";
    public BuecherController(BuecherService service)
    {
        _service = service;
    }

    [HttpGet]
    public async Task<IEnumerable<Buch>> Get()
    {
        return await _service.FindAll(table);
    }
    [HttpGet("{id}", Name = "FindOneBuch")]
    public async Task<ActionResult<Buch>> Get(int id)
    {
        var source = new CancellationTokenSource();
        var toke = source.Token;
        var result = await _service.FindOne(table, id);
        if (result != default)
        {
            return Ok(result);
        }
        return NotFound();
    }
    [HttpPost]
    public async Task<ActionResult<Buch>> Insert(BuchDto dto)
    {
        if (dto.Id != null)
        {
            return BadRequest("Id cannot be set for insert action.");
        }
        var book = new Buch();
        book.Autor = dto.Autor;
        book.Genre = dto.Genre;
        book.Verfuegbarkeit = dto.Verfuegbarkeit;
        book.Titel = dto.Titel;
        var id = await _service.Insert(table, book);
        if (id != default)
        {

            return CreatedAtRoute("FindOneBuch", new { id = id }, dto);
        }

        return BadRequest();
    }
    [HttpPut]
    public async Task<ActionResult<Buch>> Update(BuchDto dto)
    {
        if (dto.Id == null)
        {
            return BadRequest("Id should be set for insert action.");
        }
        var book = new Buch();
        book.Id = dto.Id;
        book.Autor = dto.Autor;
        book.Genre = dto.Genre;
        book.Verfuegbarkeit = dto.Verfuegbarkeit;
        book.Titel = dto.Titel;
        var id = await _service.Update(table, book);
        if (id > 0)
        {
            return CreatedAtRoute("FindOneBuch", new { id = id }, dto);
        }

        return NotFound();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Buch>> Delete(int id)
    {
        var result = await _service.Delete(table, id);
        if (result > 0)
        {
            return Ok(result);
        }

        return NotFound();
    }
}