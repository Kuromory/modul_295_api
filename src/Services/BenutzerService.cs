using Microsoft.EntityFrameworkCore;
using System.Linq;
public class BenutzerService
{

    private readonly SqlContext _dbContext;

    public BenutzerService(SqlContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task<int> Delete(string table, int id)
    {

        try
        {
            _dbContext.Benutzer.Remove(new Benutzer() { Id = id });
            return await _dbContext.SaveChangesAsync();

        }
        catch (DbUpdateConcurrencyException)
        {
            return 0;
        }
    }




    public async Task<IEnumerable<Benutzer>> FindAll(string table, Func<Benutzer, bool>? filter = null)
    {
        if (filter == null)
        {
            filter = x => true;
        }

        return _dbContext.Benutzer.Where(filter);

    }

    public async Task<Benutzer?> FindOne(string table, int id)
    {


        return await _dbContext.Benutzer.FirstOrDefaultAsync(x => x.Id == id);


    }

    public async Task<int> Insert(string table, Benutzer model)
    {
        _dbContext.Add(model);
        return await _dbContext.SaveChangesAsync();
    }

    public async Task<int> Update(string table, Benutzer model)
    {
        try
        {
            _dbContext.Update(model);
            return await _dbContext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            return 0;
        }
    }

}