using Microsoft.EntityFrameworkCore;
using System.Linq;
public class AusgelieheneBuecherService
{

    private readonly SqlContext _dbContext;

    public AusgelieheneBuecherService(SqlContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task<int> Delete(string table, int id)
    {

        try
        {
            _dbContext.AusgelieheneBuecher.Remove(new AusgeliehenesBuch() { Id = id });
            return await _dbContext.SaveChangesAsync();

        }
        catch (DbUpdateConcurrencyException)
        {
            return 0;
        }
    }




    public async Task<IEnumerable<AusgeliehenesBuch>> FindAll(string table, Func<AusgeliehenesBuch, bool>? filter = null)
    {
        if (filter == null)
        {
            filter = x => true;
        }

        return _dbContext.AusgelieheneBuecher.Where(filter);

    }

    public async Task<AusgeliehenesBuch?> FindOne(string table, int id)
    {


        return await _dbContext.AusgelieheneBuecher.FirstOrDefaultAsync(x => x.Id == id);


    }

    public async Task<int> Insert(string table, AusgeliehenesBuch model)
    {
        _dbContext.Add(model);
        return await _dbContext.SaveChangesAsync();
    }

    public async Task<int> Update(string table, AusgeliehenesBuch model)
    {
        try
        {
            _dbContext.Update(model);
            return await _dbContext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            return 0;
        }
    }

}