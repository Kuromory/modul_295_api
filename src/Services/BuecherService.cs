using Microsoft.EntityFrameworkCore;
using System.Linq;
public class BuecherService
{

    private readonly SqlContext _dbContext;

    public BuecherService(SqlContext dbContext)
    {
        _dbContext = dbContext;
    }


    public async Task<int> Delete(string table, int id)
    {

        try
        {
            _dbContext.Buecher.Remove(new Buch() { Id = id });
            return await _dbContext.SaveChangesAsync();

        }
        catch (DbUpdateConcurrencyException)
        {
            return 0;
        }
    }




    public async Task<IEnumerable<Buch>> FindAll(string table, Func<Buch, bool>? filter = null)
    {
        if (filter == null)
        {
            filter = x => true;
        }

        return _dbContext.Buecher.Where(filter);

    }

    public async Task<Buch?> FindOne(string table, int id)
    {


        return await _dbContext.Buecher.FirstOrDefaultAsync(x => x.Id == id);


    }

    public async Task<int> Insert(string table, Buch model)
    {
        _dbContext.Add(model);
        return await _dbContext.SaveChangesAsync();
    }

    public async Task<int> Update(string table, Buch model)
    {
        try
        {
            _dbContext.Update(model);
            return await _dbContext.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            return 0;
        }
    }

}