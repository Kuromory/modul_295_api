using Microsoft.EntityFrameworkCore;

public partial class SqlContext : DbContext{
    public SqlContext(DbContextOptions<SqlContext> options): base(options){}

    public virtual DbSet<Buch> Buecher {get;set;}
    public virtual DbSet<Benutzer> Benutzer  {get;set;}
    public virtual DbSet<AusgeliehenesBuch> AusgelieheneBuecher  {get;set;}

}