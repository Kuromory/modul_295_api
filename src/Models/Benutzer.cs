﻿
public class Benutzer : Model
{

    public string Name { get; set; } = string.Empty;

    public string Adresse { get; set; } = string.Empty;
    public string Kontaktnummer { get; set; } = string.Empty;

    public string Benutzername { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
}

