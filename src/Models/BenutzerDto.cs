﻿
public class BenutzerDto : Model
{

    public required string Name { get; set; } 
    public required string Adresse { get; set; }
    public required string Kontaktnummer { get; set; } 

    public required string Benutzername { get; set; } 
    public required string Password { get; set; } 
}

