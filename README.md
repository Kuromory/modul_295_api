## Bücherverwaltung für das Modul 295

Für die Datenbank wurde eine MySql Datenbank verwendet. Die Einlogdaten sollten im ``appsettings.Development.json`` File angepasst werden.

Falls eine Andere MySql Datenbank verwendet wird wie z.B. MariaDb Sollte im ``Startup.cs`` unter dem ServerType MariaDb anstelle von MySql angegeben werden (Zeile 31).

Die Dokumentation mittels ``Swagger`` kann wie folgt aufgerufen werden:

[localhost:5285/swagger](localhost:5285/swagger)

API Requests können unter folgendem Link verwendet werden:

[localhost:5285/api/Buecher](localhost:5285/api/Buecher)